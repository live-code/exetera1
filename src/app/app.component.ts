import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from './model/user';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: `
    <div class="alert alert-danger" *ngIf="error">Errore server</div>
    <pre>{{inputName.errors | json}}</pre>
    <div class="container mt-2">
      <form
        #f="ngForm"
        (submit)="saveHandler(f)"
      >
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <app-input-icon [inputModelRef]="inputName"></app-input-icon>
            </div>
          </div>
          
          <input
            class="form-control"
            [ngClass]="{
              'is-invalid': inputName.invalid && f.dirty, 
              'is-valid': inputName.valid
            }"
            placeholder="Name"
            type="text"
            name="name"
            [ngModel]
            required minlength="5" #inputName="ngModel">
        </div>

        <app-input-bar [inputError]="inputName.errors"></app-input-bar>

        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <app-input-icon [inputModelRef]="inputCity"></app-input-icon>
            </div>
          </div>
          <input
            class="form-control"
            placeholder="City"
            type="text"
            name="city"
            [ngModel] 
            #inputCity="ngModel"
            [ngClass]="{
              'is-invalid': inputCity.invalid && f.dirty, 
              'is-valid': inputCity.valid
            }"
            required
            minlength="4"
          >
        </div>

        <app-input-bar [inputError]="inputCity.errors"></app-input-bar>
        
        <select class="form-control" name="gender" [ngModel]>
          <option [ngValue]="null">Select Gender</option>
          <option value="M">male</option>
          <option value="F">female</option>
        </select>
        <button type="submit" [disabled]="f.invalid">save</button>
      </form>

      <div *ngIf="!users">loading...</div>
      <div *ngIf="!users?.length">There are no users</div>

      <li
        class="list-group-item"
        *ngFor="let user of users"
        [ngClass]="{
          'male': user.gender === 'M',
          'female': user.gender === 'F'
        }"
      >
        {{user.name}} <br>
        <i class="fa fa-trash pull-right" (click)="deleteUser(user)"></i>
        <div *ngIf="user.birthday">BirthDay: {{user.birthday | date}}</div>

        <img
          *ngIf="user.city"
          [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + user.city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'"
          alt=""
        >
      </li>
    </div>
  `,
  styles: [`
    
  `]
})
export class AppComponent implements OnInit {
  @ViewChild('inputName') inputName: ElementRef<HTMLInputElement>;
  @ViewChild('inputCity') inputCity: ElementRef<HTMLInputElement>;
  users: User[];
  error: boolean;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => this.users = res)
  }

  deleteUser(user: User) {
    this.error = false
    this.http.delete(`http://localhost:3000/users/${user.id}`)
      .subscribe(
        () => {
          const index = this.users.findIndex(u => u.id === user.id)
          this.users.splice(index, 1)
        },
        err => this.error = true
      )

  }

  saveHandler(f: NgForm) {
    this.error = false;
    const user = f.value as User;
    this.http.post<User>('http://localhost:3000/users', user)
      .subscribe(res => {
        this.users.push(res)
      })
  }

}


