import { Component, Input, OnInit } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-input-bar',
  template: `
    <div
      class="bar"
      [style.width.%]="getBarPerc(inputError?.minlength)"
      [style.background-color]="getBarColor(inputError?.minlength)"
    ></div>
  `,
  styles: [`
    .bar {
      width: 100%;
      background-color: red;
      height: 5px;
      border-radius: 10px;
      transition: all 1.3s cubic-bezier(0.68, -0.6, 0.32, 1.6)
    }
  `]
})
export class InputBarComponent  {
  @Input() inputError: ValidationErrors;


  getBarPerc(minLengthError: ValidationErrors) {
    if (!minLengthError) {
      return 0;
    }

    const perc = (minLengthError.actualLength / minLengthError.requiredLength)*100;
    return perc
  }

  getBarColor(minlength: ValidationErrors) {
    const value = this.getBarPerc(minlength);

    return value > 50 ? 'orange' : 'red'
  }
}
